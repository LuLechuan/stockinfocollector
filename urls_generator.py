import pandas as pd

filepath = "f_year.csv"
df = pd.read_csv(filepath, header=0)
names = set()
for name in df.loc[:,"Market_and_Exchange_Names"]:
    names.add(str(name))

urls = {}
for name in names:
    updated = name.replace(" ", "+")
    url = "https://www.google.com/search?q=cme+group+{}&oq=cmegroup+{}".format(updated, name)
    urls[name] = url

def getUrls():
    return urls
