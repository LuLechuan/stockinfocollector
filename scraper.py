import scrapy
import requests
import re
from parsel import Selector
import urls_generator
from threading import Lock
from boundedExecutor import BoundedExecutor

start_urls = urls_generator.getUrls()
executor = BoundedExecutor(5, 5)
lock = Lock()

def get_search_result(link):
    response = requests.get(link)
    selector = Selector(response.text)
    href_links = selector.xpath('//a/@href').getall()

    for href_link in href_links:
        if "www.cmegroup.com" in href_link:
            m = re.search('https(.+?)html', href_link)
            if m:
                return "https{}html".format(m.group(1))
    return None

def get_contract_specifications(link):
    if link:
        return link.replace(".html", "_contract_specifications.html")
    return None

def get_contract_specifications_data(link):
    if not link:
        return None
    response = requests.get(link)
    selector = Selector(response.text)
    content = selector.xpath('//td/text()').getall()
    filtered_data = None
    for text in content:
        if "CME Globex: " in text:
            filtered_data = str(text).strip()
            break
    if not filtered_data:
        return None
    data_map = {}
    m = re.search('CME Globex: (.+?)CME', filtered_data)
    if m:
        data_map["CME Globex"] = m.group(1)
    m = re.search('CME ClearPort: (.+?)Clearing', filtered_data)
    if m:
        data_map["CME ClearPort"] = m.group(1)
    m = re.search('Clearing: (.+)', filtered_data)
    if m:
        unprocessed = m.group(1)
        if "TAS:" in unprocessed:
            unprocessed = unprocessed.split("TAS:")[0]
        data_map["Clearing"] = unprocessed
    return data_map

result_data = {}
count = 0
total = len(start_urls)

def single_task(name, url):
    global count
    contract_specifications_link = get_contract_specifications(get_search_result(url))
    result = get_contract_specifications_data(contract_specifications_link)
    lock.acquire()
    result_data[name] = result
    count += 1
    print("Finishing: {}/{}".format(count, total))
    lock.release()

for name, url in start_urls.items():
    executor.submit(single_task(name, url))

import json
with open('result.json', 'w') as fp:
    json.dump(result_data, fp)
